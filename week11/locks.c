#include <stdatomic.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

pthread_mutex_t lock;
uint64_t counter = 0;
int num_threads;

struct thread_info {
    pthread_t id;
    size_t incs;
};

void* thread_locks(void* arg) {
    struct thread_info *tinfo = (struct thread_info*)arg;
    
    for (size_t i = 0; i < tinfo->incs; i++) {
        pthread_mutex_lock(&lock);
        counter += 1;
        pthread_mutex_unlock(&lock);
    }
}

void* thread_atomics(void* arg) {
    struct thread_info *tinfo = (struct thread_info*)arg;
    
    for (size_t i = 0; i < tinfo->incs; i++) {
        uint64_t clocal = counter;
        while (!atomic_compare_exchange_strong(&counter, &clocal, clocal + 1)) {
            clocal = counter;
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        printf("usage: ./locks <numthreads> <locks|atomics>\n");
        return 1;
    }
    
    num_threads = atoi(argv[1]);
    void* tfunc;

    if (!strcmp(argv[2], "locks")) {
        tfunc = &thread_locks;
    } else if (!strcmp(argv[2], "atomics")) {
        tfunc = &thread_atomics;
    } else {
        printf("need to specify locks or atomics for the threads\n");
    }
    
    pthread_mutex_init(&lock, NULL);
    struct thread_info *tinfo;
    pthread_attr_t attr;
    tinfo = calloc(num_threads, sizeof(*tinfo));
    size_t total_incs = 100000000; // 100 million

    for (size_t tnum = 0; tnum < num_threads; tnum++) {
        tinfo[tnum].id = tnum + 1;
        tinfo[tnum].incs = total_incs / num_threads;
    
        pthread_create(&tinfo[tnum].id, &attr, tfunc, &tinfo[tnum]);
    }

    for (size_t tnum = 0; tnum < num_threads; tnum++) {
        pthread_join(tinfo[tnum].id, NULL);
    }
    printf("counter is %ld\n", counter);
    return 0;
}
