#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/**
 * @brief Demonstrates process creation and execution using fork and exec.
 *
 * This program illustrates the process of creating a child process using 'fork'
 * and replacing the child process with a new program using 'execvp'. It also
 * demonstrates how the parent process waits for the child process to complete
 * and reports the child's exit status or termination by signal.
 *
 * @return Returns 0 upon successful execution.
 */
int main() {
  pid_t child_pid;

  // Fork a new process
  child_pid = fork();

  if (child_pid == 0) {
    // This code is executed by the child process

    printf("Child process executing...\n");

    // Replace the child process with a new program using exec
    char *program_name =
        "/bin/ls";  // Replace with the path to the program you want to execute
    char *args[] = {
        program_name,  // The first argument, by convention, should point to the
                       // filename associated with the file being executed.
        "-l", NULL};

    // The execvp function replaces the current process with a new program
    // specified by 'program_name'. 'args' is an array containing the program
    // name and its command-line arguments.
    if (execvp(program_name, args) < 0) {
      perror("Exec failed");
      exit(1);
    }
  } else {
    // This code is executed by the parent process

    printf("Parent process waiting for child...\n");

    // Wait for the child process to complete
    int status;
    waitpid(child_pid, &status, 0);

    if (WIFEXITED(status)) {
      printf("Child process exited with status %d\n", WEXITSTATUS(status));
    } else if (WIFSIGNALED(status)) {
      printf("Child process terminated by signal %d\n", WTERMSIG(status));
    }

    printf("Parent process exiting...\n");
  }

  return 0;
}
